const labCommand = require('./commands/lab');

module.exports = {
	name: 'Lab',

	init: function () {
		[
			'onBeforeGetClientConfig',
			'onBeforeGetCommandRoles',
			'onBeforeGetEffect',
			'onBeforeGetMapList'
		].forEach(e => {
			this.events.on(e, this[e].bind(this));
		});
	},
	
	onBeforeGetClientConfig: function (config) {
		config.clientComponents.push({
			extends: 'effects',
			path: `server/${this.relativeFolderName}/clientComponents/burnEffect.js`
		});
	},

	onBeforeGetCommandRoles: function (commandRoles, commandActions) {
		commandRoles.lab = 0;
		commandActions.lab = labCommand;
	},

	onBeforeGetEffect: function (result) {
		if (result.type.toLowerCase() === 'burn')
			result.url = `${this.relativeFolderName}/effects/effectBurn.js`;
	},

	onBeforeGetMapList: function (mapList) {
		mapList.push({
			name: 'lab',
			path: `${this.relativeFolderName}/maps`
		});
	}
};
