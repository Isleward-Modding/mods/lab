const burnEvery = 1;
const burnTtl = 5;
const burnIntensity = 1;
const tickArea = (type, update) => {
	return {
		type,

		lifetime: 0,
	
		contents: [],
	
		collisionEnter: function (o) {
			if (!o.aggro)
				return;

			this.contents.push(o);
		},
	
		collisionExit: function (o) {
			let contents = this.contents;
			let cLen = contents.length;
			for (let i = 0; i < cLen; i++) {
				if (contents[i] === o) {
					contents.splice(i, 1);
					return;
				}
			}
		},
	
		update: function () {
			this.lifetime++;

			if (this.lifetime % burnEvery !== 0) return;

			let contents = this.contents;
			for (let i = 0; i < contents.length; i++) {
				let c = contents[i];
	
				if (c) {
					// let damage = this.getDamage(c);
					// this.applyDamage(c, damage);
					update(c);
					// update.apply(this, [c]);
				}
			}
		}
	};
};

module.exports = {
	name: 'Lab',
	level: [1, 20],
	resources: {},
	objects: {
		applyburn: {
			components: {
				applyBurnEffect: tickArea('applyBurnEffect', function (o) {
					if (o.effects) {
						o.effects.addEffect({
							type: 'burn',
							ttl: burnTtl,
							amount: burnIntensity
						});
					}
				}),
				cpnParticles: {
					simplify: function () {
						return {
							type: 'particles',
							blueprint: {
								chance: 0.1,
								color: {
									start: ['d43346', 'faac45'],
									end: ['c0c3cf', '929398']
								},
								scale: {
									start: {
										min: 4,
										max: 24
									},
									end: {
										min: 0,
										max: 12
									}
								},
								spawnType: 'circle',
								lifetime: {
									min: 1,
									max: 2
								},
								spawnCircle: {
									x: 0,
									y: 0,
									r: 8
								},
								speed: {
									start: {
										min: 4,
										max: 24
									},
									end: {
										min: 0,
										max: 12
									}
								},
								randomSpeed: true,
								randomScale: true,
								randomColor: true
							}
						};
					}
				}
			}
		},
		shophermit: {
			properties: {
				cpnNotice: {
					actions: {
						enter: {
							cpn: 'dialogue',
							method: 'talk',
							args: [{
								targetName: 'hermit'
							}]
						},
						exit: {
							cpn: 'dialogue',
							method: 'stopTalk'
						}
					}
				}
			}
		},
		'sun carp school': {
			max: 9,
			type: 'fish',
			quantity: [6, 12]
		},
		fireplace: {
			components: {
				cpnWorkbench: {
					type: 'cooking'
				}
			}
		}
	},
	mobs: {
		default: {
			regular: {
				drops: {
					chance: 35,
					rolls: 1
				}
			}
		}
	}
};
