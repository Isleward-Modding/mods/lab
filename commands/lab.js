const sendObjToZone = require('../../../components/portal/sendObjToZone');

module.exports = function (command) {
	const { obj } = this;

	sendObjToZone({
		obj,
		zoneName: 'lab',
		toPos: {
			x: 30,
			y: 11
		}
	});
};
