define([
	'js/system/events',
	'js/rendering/renderer'
], function (
	events,
	renderer
) {
	const burnParticles = {
		chance: 0.1,
		color: {
			start: ['d43346', 'faac45'],
			end: ['c0c3cf', '929398']
		},
		scale: {
			start: {
				min: 4,
				max: 24
			},
			end: {
				min: 0,
				max: 12
			}
		},
		spawnType: 'circle',
		lifetime: {
			min: 1,
			max: 2
		},
		spawnCircle: {
			x: 0,
			y: 0,
			r: 8
		},
		speed: {
			start: {
				min: 4,
				max: 24
			},
			end: {
				min: 0,
				max: 12
			}
		},
		randomSpeed: true,
		randomScale: true,
		randomColor: true
	};

	const burn = {
		type: 'burn',

		emitter: null,

		init: function () {
			let blueprint = burnParticles;
			blueprint.pos = {
				x: (this.obj.x * scale) + (scale / 2),
				y: (this.obj.y * scale) + (scale / 2)
			};
			blueprint.obj = this.obj;

			this.emitter = renderer.buildEmitter(blueprint);

			this.setVisible(this.obj.isVisible);

			// this.defaultDamageText(false);

			events.emit('onGetEffectIcon', {
				id: this.id,
				icon: [2, 0]
			});

			// console.log('init', this.id, this);
		},

		setVisible: function (visible) {
			//Sometimes, we make emitters stop emitting for a reason
			// for example, when an explosion stops
			if (!this.emitter.disabled)
				this.emitter.emit = visible;
		},

		update: function () {
			const { emitter, obj } = this;

			if (this.obj.player) console.log(this.amount);

			// console.log('update', this.id, this);

			if (!emitter.emit)
				return;

			emitter.spawnPos.x = (obj.x * scale) + (scale / 2) + obj.offsetX;
			emitter.spawnPos.y = (obj.y * scale) + (scale / 2) + obj.offsetY;
		},

		destroy: function () {
			// console.log('destroy', this.id, this);

			if (!this.obj.destroyed)
				this.defaultDamageText(true);

			events.emit('onRemoveEffectIcon', {
				id: this.id
			});

			renderer.destroyEmitter(this.emitter);
		}
	};

	return {
		templates: {
			burn
		}
	};
});
