// const combat = require('../../../combat/combat');

module.exports = {
	type: 'burn',

	amount: 1,

	init: function () {
		
	},

	shouldStack: function (config) {
		return true;
	},

	incrementStack: function (config) {
		this.amount += config.amount ?? 1;

		if (config.ttl)
			this.ttl = config.ttl;
	},

	events: {
		afterTick: function () {
			this.obj.instance.syncer.queue('onGetDamage', {
				id: this.obj.id,
				event: true,
				text: `${this.amount} stacks`,
				element: 'fire'
			}, -1);

			this.syncExtend({ amount: this.amount });

			// let newDamage = combat.getDamage({
			// 	source: {
			// 		stats: {
			// 			values: {}
			// 		}
			// 	},
			// 	target: this.obj,
			// 	isAttack: false,
			// 	damage: 5,
			// 	element: 'fire',
			// 	noScale: true,
			// 	noCrit: true
			// });

			// Doesn't work without source/aggro cpn
			// #1867
			// this.obj.stats.takeDamage(newDamage, 1, this.caster);
		}
	}
};
